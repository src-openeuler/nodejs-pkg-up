%{?nodejs_find_provides_and_requires}

%global packagename pkg-up
%global enable_tests 0
# tests disabled until 'ava' is packaged in Fedora

Name:                nodejs-pkg-up
Version:             1.0.0
Release:             1
Summary:             Find the closest package.json file

License:             MIT
URL:                 https://github.com/sindresorhus/pkg-up.git
Source0:             https://registry.npmjs.org/%{packagename}/-/%{packagename}-%{version}.tgz
#git clone https://github.com/sindresorhus/pkg-up.git
#cd pkg-up
#git archive --prefix='fixture/' --format=tar ${gittag}:fixture/ \
#    | bzip2 > "$pwd"/fixture-${tag}.tar.bz2
Source1:             fixture-%{version}.tar.bz2

Source2:             https://raw.githubusercontent.com/sindresorhus/pkg-up/v%{version}/test.js

ExclusiveArch:       %{nodejs_arches} noarch
BuildArch:           noarch

BuildRequires:       nodejs-packaging npm(find-up)
%if 0%{?enable_tests}
BuildRequires:       npm(ava) npm(xo)
%endif

Requires:            nodejs

%description
Find the closest package.json file


%prep
%setup -q -n package
# setup the tests
%setup -q -T -D -a 1 -n package
cp -p %{SOURCE2} .

%nodejs_fixdep find-up

%build
# nothing to do!

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/%{packagename}
cp -pr package.json *.js \
    %{buildroot}%{nodejs_sitelib}/%{packagename}

%nodejs_symlink_deps

%check
%nodejs_symlink_deps --check
%{__nodejs} -e 'require("./")'
%if 0%{?enable_tests}
%{_bindir}/ava
%else
%{_bindir}/echo -e "\e[101m -=#=- Tests disabled -=#=- \e[0m"
%endif


%files
%{!?_licensedir:%global license %doc}
%doc *.md
%license license
%{nodejs_sitelib}/%{packagename}

%changelog
* Wed Aug 19 2020 wangxiao <wangxiao65@huawei.com> - 1.0.0-1
- package init
